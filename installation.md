`author for this document: Mathias Kirkeskov Madsen`
# Installation guide

We have a set of microservices. The source code can be found on DTU gitlab. 

## Barcode service
<https://gitlab.gbar.dtu.dk/cairo/barcode-service>

## Merchant app
<https://gitlab.gbar.dtu.dk/cairo/merchant-app>

## System tests (DTUpay)
<https://gitlab.gbar.dtu.dk/cairo/system-tests>

## User service
<https://gitlab.gbar.dtu.dk/cairo/user-service>

## Payments service
<https://gitlab.gbar.dtu.dk/cairo/payments-service>

## Customer app
<https://gitlab.gbar.dtu.dk/cairo/customer-app>

## Tools repository

Additionally we have a repository for tools that might be beneficial for development, which also contains this guide in markdown format.

<https://gitlab.gbar.dtu.dk/cairo/tools>

# Installing software on the server
This guide is for setting up your Ubuntu 18.04 server with Jenkins.

You will need to install the following:  `java`, `maven`, `jenkins`, `docker` and `docker-compose`.

After having installed the software correctly on the server, the server will:


* Pull newest commits from DTUpay Gitlab.
* Build the newest pulled commits. 
* Build the newest Docker image for the latest builds. 
* Deploy Docker containers containing the entire DTUpay system. 

Most of the software installed is described without a version in mind, due to their history of being backwards compatible. In case this no longer holds, this guide will be updated accordingly. If the version of installed Ubuntu version on the server is updated, this guide will be updated. 

## Connect to the server
We assume you have already have access to a server that contains a clean install of Ubuntu 18.04, which you can connect to through ssh. We also assume that all ports are open per default. 

Connect to the server using ssh using a password or keyfile
```bash
  $ ssh -i <path-to-keyfile> <name>@<server-address> #using keyfile
  $ ssh <name>@<server-address> #using password
```
Where the name is your username, which can in some cases be root. 

For all the installs it assumed that you are a root user. If you are not a root user, you should use `sudo` before every command.

## Installing git
We used 2.19.1, but newer versions should be backwards compatible. 
```bash
  $ apt install git
```

## Installing java 
```bash
apt update
apt install openjdk-8-jdk
```

## Installing maven
```bash
apt install maven
```

## Installing docker
We used 18.06.1-ce, but newer versions should be backwards compatible. 

First install some things that docker depends on.
```bash
apt install apt-transport-https ca-certificates curl software-properties-common
```
Then add a GPG keys to the official Docker repository, which allows authenticated downloads. 

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Add the Docker repository to APT sources in order to allow you to install it with APT, and then install it with apt. 

```bash
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt install docker-ce
```

Add apropriate users to the Docker usergroup. These users will now be able to manage containers.
```bash
sudo usermod -aG docker <user>
```
If you are logged in and want access rights to docker, either log out and in or restart your server. 

A detailed guide can be found at <https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04>

## Installing docker-compose
Download release 1.21.2 from the official release of docker compose in github and move it to your local bin folder using the following command. This will automatically download for your cpu architecture and os. 
```bash
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

## Installing jenkins
It is assumed that docker and jdk8 has already been installed before installing Jenkins.

We start out by downloading jenkins from jenkins.io, with the following commands.
```bash
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
```

After doing this, Jenkins is now added to the apt repository and can be installed with the following commands.

```bash
apt update
apt install jenkins
```

Jenkins is now installed. Before starting Jenkins, we will add the Jenkins user to the docker user group. 
```bash
sudo usermod -aG docker jenkins
```

Now we can start Jenkins with the following command!
```bash
sudo systemctl start jenkins
```
In a browser, go to <http://localhost:8080/>. You should insert the password which can be found with the following command.
```bash
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```
Now press **select plugins** and tick **gitlab** in additonal to the default, and press continue. 

Wait for the plugins to install, and then create your first admin account. **Remember to make a secure password**. 

Finaly, change the default port from 8080 to 8282 by editing the following file, and then restart jenkins.
```bash
sudo gedit /etc/default/jenkins
sudo systemctl restart jenkins
```
Congratulations, jenkins is now ready on `<addr>:8282`


## Create a dashboard for a service on Jenkins
In order to create a dashboard, press **new item -> Freestyle project** enter the name and press **ok**.

This will send you to the configuration, where you should tick the box under **Source Code Management** that says **git**. Enter the repository you want in this Jenkins project, and insert proper credentials if the repository is not public. Credentials can be generated by pressing the "add"-button and inserting your gitlab login into the pop-up. If this succeeded, move down to **Build triggers** and tick **"Build when a change is pushed to GitLab. GitLab webhook can be seen on the option ticked and has the general structure: URL: http://ADDR:PORT/project/NAME"**

Then go into your gitlab repository and insert the hook (the url given in jenkins) into your project. You do this by entering your gitlab project, clicking on **settings**, **integrations** and then insert the url into the first url box, make sure that the box **"Push events"** is ticked and press **"Add webhook"**.
Optionally, more security is possible by using a secret token to ensure that not anybody can trigger the hook. This is done by going down to **Build triggers** again and pressing the **Advanced button**, the secret token generator is in the bottom of the expanded advanced options. This token can be inserted in the webhook below the URL previously inserted.

Now go back into your jenkins settings, scroll down to \textbf{"Build"} and select \textbf{"Execute shell"} from the **"Add build step"** dropdown. In this build step, enter `bash jenkins.sh`. Inside `jenkins.sh` for every existing microservice is the shell scripts that are run every time the project is pushed. 
Jenkins.sh contains important docker commands and maven commands nescessary for executing the code.

Congratulations! You now have a functioning jenkins service that will execute jenkins.sh every time a new commit is pushed to your repository on gitlab! ❤

You can do this for every service, and will now have a fully fledged test environment - The last thing you need is an Andon board!

## Create a new view
At the top above your services, press the "`+`" to create a new view. 

Select build view, and then select all the services you want to monitor and continue.

You now have a nice andon board that provides overview of the latest build of all services!


## Deployment
While the software will be automatically deployed with the jenkins configurations, it should be noted that we use Docker and Docker-Compose to deploy our software. 

In our system-tests repository a `docker-compose.yaml` file can be found. Running this file with the following command will start the entire service, and all its microservices. 
```bash
docker-compose up
```
Additionally, when changes are made to the repository the docker containers should be rebuilt with the following command
```bash
docker-compose build
```

# Development software

We used a set of tools for developing DTUpay, which you might find handy.

This guide is aimed at `Ubuntu 18.10`. 

A set of tools that is also used on the server should be installed. These are `git`, `java`, `maven`, `rabbitMQ`,`docker` and `docker-compose`. With the exception of java, these have been pretty backwards-compatible, making the newest version a viable choice. **Installation guide for these are identical for the ones on the server.**

Besides the stuff also used on the server, we use **IntelliJ Community**, **Visual Studio Code** and **Postman**.

## Downloading repositories

Set up credentials or ssh keys with your git client, and then clone the desired repository with
```bash
git clone <url>
```
The url, either http or ssh, can be found at the beginning of this document. 


## Installing IntelliJ
In order to develop our services in java, we used IntelliJ, as it provides a nice toolset for development.

Go to <https://www.jetbrains.com/idea/download/index.html#section=linux> and download IntelliJ community. We used version 2018.3.3.  

Change directory into the Downloads folder 
```bash
cd $HOME/Downloads
```

Unpack the file using 
```bash 
sudo tar xf idea-IC-183-5153.38.tar.gz -C /opt/
```

```bash
cd /opt/idea-IC-183-5153.38/bin
./idea.sh
```
Choose default installation settings

Accept terms

Send or don't send usage statistics 

Choose or skip settings. Choose default plugins. 

Finalize

Congratulations! You now have IntelliJ installed!

### Import a project
Open IntelliJ

Choose "Import a project"

Find the path to where your git repository is. 

Congratulations! You can now develop on the DTUpay!

## Installing vscode

We used vscode primarily for quick edits.

Start out by downloading it from <https://code.visualstudio.com/download>. We downloaded version 1.30.2 for amd64. 

Install it by using the following commands, which will extract the package and then install missing dependencies.
```bash
sudo dpkg -i code_*.deb
sudo apt install -f
```
Congratulations, you can now use Visual Studio Code!

## Installing postman

Postman can be installed by using `snap`, which comes with Ubuntu. We installed version 6.7.1.

```bash
sudo snap install postman
```
You have now installed postman, and can create your requests! Congratulations!


## RabbitMQ
Rabbitmq is the message queue library. Rabbitmq uses two ports, 5672 is the port is does the actual message queueing through, while 15672 is the management UI which is very nice for monitoring the queues activity.
For the management UI, we added the following to the jenkins.sh file.
```bash
docker run -d --hostname myrabbit --name somerabbit -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```
It can be accessed by using the jenkins url with the 15672 port. The login is name:guest and pswrd:guest as default.

