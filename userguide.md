`author for this document: Daniel Larsen`
# User guide customer
Hi there!
Thank you for checking out our project "DTUPay"
If you are a customer, then these are your following functions that you can use to interact with our app:

- `requestTokens(String amount)`

    This HTTP request an X amount of tokens from the system. You can request between 1-5 tokens and hold up to 6.
    These tokens can be used to conduct payment with your local merchant.


- `createUser(User user, boolean isCustomer)`

    This function registers you(Customer) in DTUPay's database if isCustomer is "True".


- `deleteUser(String cpr)`

    This function deletes you from the database that you are registeret in. You just have to give it your cpr-number.

- `URL:8484/barcode/{tokenid}`

    DTUPay also implements a barcode generator. If you type in the url above with a tokenId, you will recieve a barcode image on your page.


# User guide merchant

Hi there!
Thank you for checking out our project "DTUPay"
If you are a merchant, then these are your following functions that you can use to interact with our app:

- `createMerchant(WebTarget webTarget, Merchant user, ArrayList<String> testUserCprs)`

    This function creates a merchant in the system


- `deleteUser(WebTarget webTarget, String cpr)`

    This function deletes merchant in the system

- `POST :8181/payment`

    This post makes a payment and is given a JSON object with a PayRequest that contains mechantId, tokenId, amount.