#!/bin/bash

ACCOUNT_A=$(bash getAccountCpr 00010001)
ACCOUNT_B=$(bash getAccountCpr 00010002)

ADDR_A="$(echo $ACCOUNT_A| jq '.id')"
ADDR_B="$(echo $ACCOUNT_B| jq '.id')"

bash deleteAccount $(echo $ADDR_A | tr -d '"')
bash deleteAccount $(echo $ADDR_B | tr -d '"')

echo $ADDR_A
echo $ADDR_B